import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"

@Entity()
export class Product {

    @PrimaryGeneratedColumn({name: "product_id"})
    id: number

    @Column({name: "product_name"})
    name: string

    @Column({name: "product_price"})
    price: number

}
